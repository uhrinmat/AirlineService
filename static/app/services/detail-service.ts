import {Injectable, Inject} from "angular2/angular2";
import {HTTP_PROVIDERS, Http, Request, Response, RequestMethods, RequestOptions} from "angular2/http";
import {CommonService} from "../components/common/common";

@Injectable()
export class DetailService {
    http:Http;
    commonService:CommonService;

    constructor(http:Http, commonService:CommonService) {
        this.http = http;
        this.commonService = commonService;
    }

    saveDetail(detail:EntityDetail, entityEndpoint:string):Promise<void> {
        var requestOptions:RequestOptions = new RequestOptions({
            method: detail.getEntityId() ? RequestMethods.Put : RequestMethods.Post,
            url: entityEndpoint + (detail.getEntityId() ? '/'+detail.getEntityId() : ''),
            body: JSON.stringify(detail)
        });
        console.log("Saving detail. Endpoint url: \"" + entityEndpoint + "\". Method: " + requestOptions.method);

        return new Promise<void>((resolve, reject) => {
            this.http.request(new Request(requestOptions)).subscribe(() => {
                // TODO redirect
                console.log("Successfully saved flight.");
                resolve();
            }, () => {
                console.log("An error occurred while saving.");
                reject();
            });
        });
    };

    fetchDetail(entityId:string|number, entityEndpoint:string,  type): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.get(entityEndpoint + '/' + entityId).subscribe((response:Response) => {
                var obj = new type();
                var responseEntity = response.json();
                this.commonService.copyOwnObjectProperties(responseEntity, obj);
                resolve(obj);
            }, () => {
                alert("Loading entity failed!");
                reject();
            });
        });
    }
}

export interface EntityDetail {
    getEntityId() : number;
}

export abstract class BaseEntityDetail implements EntityDetail {
    id:number;

    getEntityId():number {
        return this.id;
    }
}