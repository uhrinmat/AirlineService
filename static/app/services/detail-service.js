var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var angular2_1 = require("angular2/angular2");
var http_1 = require("angular2/http");
var common_1 = require("../components/common/common");
var DetailService = (function () {
    function DetailService(http, commonService) {
        this.http = http;
        this.commonService = commonService;
    }
    DetailService.prototype.saveDetail = function (detail, entityEndpoint) {
        var _this = this;
        var requestOptions = new http_1.RequestOptions({
            method: detail.getEntityId() ? http_1.RequestMethods.Put : http_1.RequestMethods.Post,
            url: entityEndpoint + (detail.getEntityId() ? '/' + detail.getEntityId() : ''),
            body: JSON.stringify(detail)
        });
        console.log("Saving detail. Endpoint url: \"" + entityEndpoint + "\". Method: " + requestOptions.method);
        return new Promise(function (resolve, reject) {
            _this.http.request(new http_1.Request(requestOptions)).subscribe(function () {
                // TODO redirect
                console.log("Successfully saved flight.");
                resolve();
            }, function () {
                console.log("An error occurred while saving.");
                reject();
            });
        });
    };
    ;
    DetailService.prototype.fetchDetail = function (entityId, entityEndpoint, type) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(entityEndpoint + '/' + entityId).subscribe(function (response) {
                var obj = new type();
                var responseEntity = response.json();
                _this.commonService.copyOwnObjectProperties(responseEntity, obj);
                resolve(obj);
            }, function () {
                alert("Loading entity failed!");
                reject();
            });
        });
    };
    DetailService = __decorate([
        angular2_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, common_1.CommonService])
    ], DetailService);
    return DetailService;
})();
exports.DetailService = DetailService;
var BaseEntityDetail = (function () {
    function BaseEntityDetail() {
    }
    BaseEntityDetail.prototype.getEntityId = function () {
        return this.id;
    };
    return BaseEntityDetail;
})();
exports.BaseEntityDetail = BaseEntityDetail;
//# sourceMappingURL=detail-service.js.map