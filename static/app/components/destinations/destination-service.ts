import {Injectable} from 'angular2/angular2';
import {Http, Response} from 'angular2/http';
import {IdName, API_URL, CommonService} from "../common/common";
import {BaseEntityDetail} from '../../services/detail-service';

@Injectable()
export class DestinationService {
    private static DESTINATION_URL = API_URL + '/destination';

    private http:Http;
    private commonService:CommonService;

    constructor(http:Http, commonService:CommonService) {
        this.http = http;
        this.commonService = commonService;
    }

    loadDestinationOptions():Promise<IdName<number>[]> {
        return new Promise<IdName<number>[]>((resolve, reject) => {
            this.loadDestinations().then((destinations) => {
                resolve(destinations.map((destination) => {
                    return new IdName(destination.id, destination.name)
                }));
            });
        });
    }

    loadDestinations():Promise<Destination[]> {
        return new Promise<Destination[]>((resolve, reject) => {
            this.http.get(DestinationService.DESTINATION_URL)
                .subscribe((result:Response) => {
                    var destinations:Object[] = <Object[]>result.json();

                    resolve(destinations.map((item) => {
                        var destination:Destination = new Destination();
                        this.commonService.copyOwnObjectProperties(item, destination);
                        return destination;
                    }));
                });
        });
    }
}

export class Destination extends BaseEntityDetail {
    name:string;
    lat:number;
    lon:number;

    constructor(name?:string, lat?:number, lon?:number) {
        super();
        this.name = name;
        this.lat = lat;
        this.lon = lon;
    }
}