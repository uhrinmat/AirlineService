var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var angular2_1 = require('angular2/angular2');
var http_1 = require('angular2/http');
var common_1 = require("../common/common");
var detail_service_1 = require('../../services/detail-service');
var DestinationService = (function () {
    function DestinationService(http, commonService) {
        this.http = http;
        this.commonService = commonService;
    }
    DestinationService.prototype.loadDestinationOptions = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.loadDestinations().then(function (destinations) {
                resolve(destinations.map(function (destination) {
                    return new common_1.IdName(destination.id, destination.name);
                }));
            });
        });
    };
    DestinationService.prototype.loadDestinations = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(DestinationService.DESTINATION_URL)
                .subscribe(function (result) {
                var destinations = result.json();
                resolve(destinations.map(function (item) {
                    var destination = new Destination();
                    _this.commonService.copyOwnObjectProperties(item, destination);
                    return destination;
                }));
            });
        });
    };
    DestinationService.DESTINATION_URL = common_1.API_URL + '/destination';
    DestinationService = __decorate([
        angular2_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, common_1.CommonService])
    ], DestinationService);
    return DestinationService;
})();
exports.DestinationService = DestinationService;
var Destination = (function (_super) {
    __extends(Destination, _super);
    function Destination(name, lat, lon) {
        _super.call(this);
        this.name = name;
        this.lat = lat;
        this.lon = lon;
    }
    return Destination;
})(detail_service_1.BaseEntityDetail);
exports.Destination = Destination;
//# sourceMappingURL=destination-service.js.map