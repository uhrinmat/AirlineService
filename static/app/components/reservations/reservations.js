var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var angular2_1 = require('angular2/angular2');
var router_1 = require('angular2/router');
var detail_service_1 = require("../../services/detail-service");
var common_1 = require('../common/common');
var reservation_service_1 = require("./reservation-service");
var ReservationsComponent = (function () {
    function ReservationsComponent(reservationService) {
        var _this = this;
        this.title = 'Reservations';
        this.reservations = [];
        reservationService.loadReservations().then(function (reservations) { return _this.reservations = reservations; });
    }
    ReservationsComponent = __decorate([
        angular2_1.Component({
            selector: 'reservations',
            directives: [router_1.RouterLink, angular2_1.NgFor],
            providers: [detail_service_1.DetailService, reservation_service_1.ReservationService],
            templateUrl: 'app/components/reservations/reservations.html'
        }), 
        __metadata('design:paramtypes', [reservation_service_1.ReservationService])
    ], ReservationsComponent);
    return ReservationsComponent;
})();
exports.ReservationsComponent = ReservationsComponent;
var ReservationComponent = (function () {
    function ReservationComponent(routeParams, detailService) {
        var _this = this;
        this.detailService = detailService;
        this.title = 'Reservation detail';
        this.reservation = new reservation_service_1.Reservation();
        this.flightOptions = []; // TODO
        var entityId = routeParams.get("id");
        if (entityId) {
            detailService.fetchDetail(entityId, ReservationComponent.ENTITY_ENDPOINT, reservation_service_1.Reservation).then(function (result) {
                _this.reservation = result;
            });
        }
    }
    ReservationComponent.prototype.saveReservation = function (reservation) {
        console.log("saving reservation ...");
        this.detailService.saveDetail(reservation, ReservationComponent.ENTITY_ENDPOINT);
    };
    ReservationComponent.ENTITY_ENDPOINT = common_1.API_URL + "/reservation";
    ReservationComponent = __decorate([
        angular2_1.Component({
            selector: 'reservation',
            directives: [angular2_1.FORM_DIRECTIVES, router_1.RouterLink, angular2_1.NgFor, angular2_1.NgIf],
            providers: [detail_service_1.DetailService],
            templateUrl: 'app/components/reservations/reservation.html'
        }), 
        __metadata('design:paramtypes', [router_1.RouteParams, detail_service_1.DetailService])
    ], ReservationComponent);
    return ReservationComponent;
})();
exports.ReservationComponent = ReservationComponent;
//# sourceMappingURL=reservations.js.map