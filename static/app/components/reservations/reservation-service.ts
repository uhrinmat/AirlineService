import {Injectable} from 'angular2/angular2';
import {Http, Response} from 'angular2/http';
import {IdName, API_URL, CommonService} from "../common/common";
import {BaseEntityDetail} from '../../services/detail-service';

@Injectable()
export class ReservationService {
    private static DESTINATIONS_URL = API_URL + '/reservation';

    private http:Http;
    private commonService:CommonService;

    constructor(http:Http, commonService:CommonService) {
        this.http = http;
        this.commonService = commonService;
    }

    loadReservations():Promise<Reservation[]> {
        return new Promise<Reservation[]>((resolve, reject) => {
            this.http.get(ReservationService.DESTINATIONS_URL)
                .subscribe((result:Response) => {
                    var reservations:Object[] = <Object[]>result.json();

                    resolve(reservations.map((item) => {
                        var reservation:Reservation = new Reservation();
                        this.commonService.copyOwnObjectProperties(item, reservation);
                        return reservation;
                    }));
                });
        });
    }
}

export class Reservation extends BaseEntityDetail {
    flightNumber:number;
    password:string;
    seats:number;
    state:string;
    created:string;

    constructor() {
        super();
    }
}