var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var angular2_1 = require('angular2/angular2');
exports.API_URL = 'http://private-2141b-aossemestralwork.apiary-mock.com';
var Pair = (function () {
    function Pair(first, second) {
        this.firstValue = first;
        this.secondValue = second;
    }
    return Pair;
})();
exports.Pair = Pair;
var IdName = (function () {
    function IdName(id, name) {
        this.id = id;
        this.name = name;
    }
    return IdName;
})();
exports.IdName = IdName;
var CommonService = (function () {
    function CommonService() {
    }
    CommonService.prototype.copyOwnObjectProperties = function (from, to) {
        for (var field in from) {
            if (from.hasOwnProperty(field)) {
                to[field] = from[field];
            }
        }
    };
    CommonService = __decorate([
        angular2_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], CommonService);
    return CommonService;
})();
exports.CommonService = CommonService;
var messageService = (function () {
    function messageService() {
        this.messages = [];
        this.messageChanged = new angular2_1.EventEmitter();
    }
    messageService.prototype.add = function (text, cssClass) {
        var message = new Message(text, cssClass);
        this.messages.push();
        this.messageChanged.next(message);
    };
    messageService.prototype.removeAll = function () {
        this.messages = [];
    };
    messageService.prototype.pickAll = function () {
        var _this = this;
        // TODO revise this
        return new Promise(function (resolve) {
            _this.messageChanged.observer(function (value) {
                resolve(_this.messages);
                _this.removeAll();
            });
        });
    };
    messageService = __decorate([
        angular2_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], messageService);
    return messageService;
})();
exports.messageService = messageService;
var XHeadersFilter = (function () {
    function XHeadersFilter() {
        this.filtered = new angular2_1.EventEmitter();
    }
    XHeadersFilter.prototype.filter = function () {
        var _this = this;
        var headers = {};
        this.columns.forEach(function (column) {
            if (column.value) {
                _this.addHeaders(headers, XHeadersFilter.X_FILTER_HEADER, column.createFilterString());
            }
            if (column.orderDirection) {
                _this.addHeaders(headers, XHeadersFilter.X_ORDER_HEADER, column.createOrderByString());
            }
        });
        console.log("filter value changed");
        this.filtered.next(headers);
    };
    XHeadersFilter.prototype.changeOrderDirection = function (columnFilter) {
        // define order transitions
        switch (columnFilter.orderDirection) {
            case XHeadersFilter.FILTER_DIRECTION_ASC:
                columnFilter.orderDirection = XHeadersFilter.FILTER_DIRECTION_DESC;
                break;
            case XHeadersFilter.FILTER_DIRECTION_DESC:
                columnFilter.orderDirection = XHeadersFilter.FILTER_DIRECTION_NONE;
                break;
            default:
                columnFilter.orderDirection = XHeadersFilter.FILTER_DIRECTION_ASC;
        }
    };
    XHeadersFilter.prototype.addHeaders = function (headers, header, value) {
        headers[header] = !headers[header] ? value : headers[header] + ',' + value;
    };
    XHeadersFilter.FILTER_DIRECTION_NONE = null;
    XHeadersFilter.FILTER_DIRECTION_ASC = "asc";
    XHeadersFilter.FILTER_DIRECTION_DESC = "desc";
    XHeadersFilter.X_FILTER_HEADER = 'X-Filter';
    XHeadersFilter.X_ORDER_HEADER = 'X-Order';
    XHeadersFilter.X_BASE_HEADER = 'X-Base';
    XHeadersFilter.X_OFFSET_HEADER = 'X-Offset';
    XHeadersFilter = __decorate([
        angular2_1.Component({
            selector: 'x-headers-filter',
            templateUrl: 'app/components/common/x-header-filter.html',
            directives: [angular2_1.FORM_DIRECTIVES, angular2_1.NgFor, angular2_1.NgIf],
            inputs: ["columns"],
            outputs: ['filtered']
        }), 
        __metadata('design:paramtypes', [])
    ], XHeadersFilter);
    return XHeadersFilter;
})();
exports.XHeadersFilter = XHeadersFilter;
var ColumnFilters;
(function (ColumnFilters) {
    var ColumnFilter = (function () {
        function ColumnFilter(name) {
            this.name = name;
            this.orderDirection = null;
            this.type = null;
            this.value = null;
        }
        ColumnFilter.prototype.createOrderByString = function () {
            return this.orderDirection ? this.name + ":" + this.orderDirection : null;
        };
        return ColumnFilter;
    })();
    ColumnFilters.ColumnFilter = ColumnFilter;
    var StringColumnFilter = (function (_super) {
        __extends(StringColumnFilter, _super);
        function StringColumnFilter(name) {
            _super.call(this, name);
            this.type = "text";
        }
        StringColumnFilter.prototype.createFilterString = function () {
            return this.name + ':%' + this.value + '%';
        };
        return StringColumnFilter;
    })(ColumnFilter);
    ColumnFilters.StringColumnFilter = StringColumnFilter;
    var DateTimeColumnFilter = (function (_super) {
        __extends(DateTimeColumnFilter, _super);
        function DateTimeColumnFilter(name) {
            _super.call(this, name);
            this.type = "datetime-local";
        }
        DateTimeColumnFilter.prototype.createFilterString = function () {
            return this.name + ':' + this.value;
        };
        return DateTimeColumnFilter;
    })(ColumnFilter);
    ColumnFilters.DateTimeColumnFilter = DateTimeColumnFilter;
    var NumberColumnFilter = (function (_super) {
        __extends(NumberColumnFilter, _super);
        function NumberColumnFilter(name) {
            _super.call(this, name);
            this.type = "number";
            this.equalityOperator = "=";
        }
        NumberColumnFilter.prototype.createFilterString = function () {
            return this.name + ":" + this.equalityOperator + this.value;
        };
        return NumberColumnFilter;
    })(ColumnFilter);
    ColumnFilters.NumberColumnFilter = NumberColumnFilter;
})(ColumnFilters = exports.ColumnFilters || (exports.ColumnFilters = {}));
var Message = (function () {
    function Message(text, cssClass) {
        this.text = text;
        this.cssClass = cssClass || "info";
    }
    return Message;
})();
//# sourceMappingURL=common.js.map