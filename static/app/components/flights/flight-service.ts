import {Injectable} from 'angular2/angular2';
import {Http, Response, Headers} from 'angular2/http';
import {BaseEntityDetail} from "../../services/detail-service";
import {API_URL} from '../common/common';

@Injectable()
export class FlightService {
    static ENTITY_ENDPOINT:string = API_URL + "/flight";

    private http:Http;

    constructor(http:Http) {
        this.http = http;
    }

    loadFlights(headers?:Headers):Promise<Flight[]> {
        return new Promise<Flight[]>((resolve) => {
            this.http.get(FlightService.ENTITY_ENDPOINT, {headers: headers}).subscribe((response:Response) => {
                resolve(<Flight[]> response.json());
            }, () => {
                alert("Problem occurred while loading flights");
            });
        });
    }
}

export class Flight extends BaseEntityDetail {
    name:string;
    dateOfDeparture:String;
    distance:number;
    seats:number;
    price:number;
    fromDestinationId:number;
    toDestinationId:number;

    getEntityId():number {
        return this.id;
    }

    constructor() {
        super();
    }
}