var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var angular2_1 = require('angular2/angular2');
var router_1 = require('angular2/router');
var detail_service_1 = require("../../services/detail-service");
var common_1 = require('../common/common');
var destination_service_1 = require("../destinations/destination-service");
var flight_service_1 = require("./flight-service");
var FlightsComponent = (function () {
    function FlightsComponent(flightService, destinationService) {
        var _this = this;
        this.flightService = flightService;
        this.title = 'Flights';
        this.flights = [];
        this.destinationMap = {};
        destinationService.loadDestinationOptions().then(function (destinationOptions) {
            destinationOptions.forEach(function (destinationOption) { return _this.destinationMap[destinationOption.id] = destinationOption.name; });
        });
        flightService.loadFlights().then(function (flights) { return _this.flights = flights; });
        this.buildColumnFilter();
    }
    FlightsComponent.prototype.onFilterChanged = function (newFilterHeaders) {
        var _this = this;
        this.flightService.loadFlights(newFilterHeaders).then(function (flights) { return _this.flights = flights; });
    };
    FlightsComponent.prototype.buildColumnFilter = function () {
        this.filterDefinition = [];
        this.filterDefinition.push(new common_1.ColumnFilters.StringColumnFilter("name"));
        this.filterDefinition.push(new common_1.ColumnFilters.DateTimeColumnFilter("dateOfDepartureFrom"));
        this.filterDefinition.push(new common_1.ColumnFilters.DateTimeColumnFilter("dateOfDepartureTo"));
        this.filterDefinition.push(new common_1.ColumnFilters.NumberColumnFilter("distance"));
        this.filterDefinition.push(new common_1.ColumnFilters.NumberColumnFilter("seats"));
    };
    FlightsComponent = __decorate([
        angular2_1.Component({
            selector: 'flights',
            directives: [angular2_1.FORM_DIRECTIVES, router_1.RouterLink, angular2_1.NgFor, common_1.XHeadersFilter],
            providers: [flight_service_1.FlightService, destination_service_1.DestinationService],
            templateUrl: 'app/components/flights/flights.html'
        }), 
        __metadata('design:paramtypes', [flight_service_1.FlightService, destination_service_1.DestinationService])
    ], FlightsComponent);
    return FlightsComponent;
})();
exports.FlightsComponent = FlightsComponent;
var FlightComponent = (function () {
    function FlightComponent(routeParams, detailService, destinationService) {
        var _this = this;
        this.detailService = detailService;
        this.destinationService = destinationService;
        this.title = 'Flight detail';
        this.flight = new flight_service_1.Flight();
        this.destinationOptions = [];
        destinationService.loadDestinationOptions().then(function (destinationOptions) {
            _this.destinationOptions = destinationOptions;
        });
        var entityId = routeParams.get("id");
        if (entityId) {
            detailService.fetchDetail(entityId, flight_service_1.FlightService.ENTITY_ENDPOINT, flight_service_1.Flight).then(function (result) {
                _this.flight = result;
            });
        }
    }
    FlightComponent.prototype.saveFlight = function (flight) {
        console.log("saving flight ...");
        this.detailService.saveDetail(flight, flight_service_1.FlightService.ENTITY_ENDPOINT);
    };
    FlightComponent = __decorate([
        angular2_1.Component({
            selector: 'flight',
            directives: [router_1.RouterLink, angular2_1.NgFor, angular2_1.FORM_DIRECTIVES],
            providers: [detail_service_1.DetailService, destination_service_1.DestinationService],
            templateUrl: 'app/components/flights/flight.html'
        }), 
        __metadata('design:paramtypes', [router_1.RouteParams, detail_service_1.DetailService, destination_service_1.DestinationService])
    ], FlightComponent);
    return FlightComponent;
})();
exports.FlightComponent = FlightComponent;
//# sourceMappingURL=flights.js.map