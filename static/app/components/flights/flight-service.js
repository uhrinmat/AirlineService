var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var angular2_1 = require('angular2/angular2');
var http_1 = require('angular2/http');
var detail_service_1 = require("../../services/detail-service");
var common_1 = require('../common/common');
var FlightService = (function () {
    function FlightService(http) {
        this.http = http;
    }
    FlightService.prototype.loadFlights = function (headers) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(FlightService.ENTITY_ENDPOINT, { headers: headers }).subscribe(function (response) {
                resolve(response.json());
            }, function () {
                alert("Problem occurred while loading flights");
            });
        });
    };
    FlightService.ENTITY_ENDPOINT = common_1.API_URL + "/flight";
    FlightService = __decorate([
        angular2_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], FlightService);
    return FlightService;
})();
exports.FlightService = FlightService;
var Flight = (function (_super) {
    __extends(Flight, _super);
    function Flight() {
        _super.call(this);
    }
    Flight.prototype.getEntityId = function () {
        return this.id;
    };
    return Flight;
})(detail_service_1.BaseEntityDetail);
exports.Flight = Flight;
//# sourceMappingURL=flight-service.js.map